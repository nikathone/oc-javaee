-- drop database if exists example_docker_db;
-- create database javaee;
use javaee;

--
-- Table structure for table `noms`
--

DROP TABLE IF EXISTS `noms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `noms` (
  id INT( 11 ) NOT NULL AUTO_INCREMENT,
  nom  VARCHAR ( 200 ) NOT NULL,
  prenom VARCHAR ( 200) NOT NULL,
  PRIMARY KEY ( id )
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `NOMS`
--

LOCK TABLES `noms` WRITE;
/*!40000 ALTER TABLE `noms` DISABLE KEYS */;
INSERT INTO `noms` (nom, prenom)
VALUES
( 'docker', 'test'),
( 'another', 'name');
/*!40000 ALTER TABLE `noms` ENABLE KEYS */;
UNLOCK TABLES;
