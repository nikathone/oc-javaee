package com.octest.servlets;

import com.octest.bdd.Noms;
import com.octest.beans.Author;
import com.octest.beans.People;
import com.octest.forms.ConnectionForm;

import java.io.IOException;

import javax.servlet.http.*;
import javax.servlet.ServletException;

public class Test extends HttpServlet {
    private static final long serialVersionUID = 1L;

    public Test() {
        super();
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String nom = request.getParameter("nom");
//        request.setAttribute("nom", nom);
//        ConnectionForm form = new ConnectionForm();
//
//        form.verifierIdentifiant(request);
//        request.setAttribute("form", form);

//        String nom = request.getParameter("nom");
//        String prenom = request.getParameter("prenom");

//        HttpSession session = request.getSession();
//        session.setAttribute("nom", nom);
//        session.setAttribute("prenom", prenom);

//        Cookie cookie = new Cookie("cookiePrenom", prenom);
//        cookie.setMaxAge(60 * 60);
//        response.addCookie(cookie);

        People user = new People();
        user.setFirstname(request.getParameter("nom"));
        user.setLastname(request.getParameter("prenom"));

        Noms tableNames = new Noms();
        tableNames.addUser(user);

        request.setAttribute("users", tableNames.getUsers());

        this.getServletContext()
                .getRequestDispatcher("/WEB-INF/bonjour.jsp")
                .forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        String message = "Au revoir !";
//        request.setAttribute("variable", message);
//
//        String name = request.getParameter("name");
//        request.setAttribute("name", name);
//
//        Author auteur = new Author();
//        auteur.setName("Nia");
//        auteur.setLastname("Katone");
//        auteur.setActive(true);
//        request.setAttribute("author",auteur);
//
//        String[] titres = { "Nouvel incendie hkjhkjhkj", "Chute du dollar gfghdfgfgd" };
//        request.setAttribute("titres", titres);

        // Retrieving cookies.
//        Cookie[] cookies = request.getCookies();
//        if (cookies != null) {
//            for (Cookie cookie : cookies) {
//                if (cookie.getName().equals("cookiePrenom")) {
//                    request.setAttribute("cookiePrenom", cookie.getValue());
//                }
//            }
//        }

        Noms tableNames = new Noms();
        request.setAttribute("users", tableNames.getUsers());

        this.getServletContext()
                .getRequestDispatcher("/WEB-INF/bonjour.jsp")
                .forward(request, response);
    }
}
