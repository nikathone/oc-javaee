package com.octest.bdd;

import com.octest.beans.People;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class Noms {
  private Connection connection;

  public List<People> getUsers() {
      List<People> users = new ArrayList<>();
      Statement statement = null;
      ResultSet result = null;

      loadDatabase();

      try {
          statement = connection.createStatement();
          // Exécution de la requête
          result = statement.executeQuery("SELECT nom, prenom FROM noms;");

          // Récupération des données
          while (result.next()) {
              String nom = result.getString("nom");
              String prenom = result.getString("prenom");

              People user = new People();
              user.setFirstname(nom);
              user.setLastname(prenom);

              users.add(user);
          }

      }
      catch (SQLException e) {
      }
      finally {
          // Fermeture de la connexion
          try {
              if (result != null)
                  result.close();
              if (statement != null)
                  statement.close();
              if (connection != null)
                  connection.close();
          } catch (SQLException ignore) {
          }
      }

      return  users;
  }

  private void loadDatabase() {
      try {
          Class.forName("com.mysql.jdbc.Driver");
      }
      catch (ClassNotFoundException e) {}

      try {
          connection = DriverManager.getConnection("jdbc:mysql://db:3306/javaee", "root", "root");
      }
      catch (SQLException e) {
          e.printStackTrace();
      }
  }

    public void addUser(People user) {
        loadDatabase();

        try {
            PreparedStatement preparedStatement = connection.prepareStatement("INSERT INTO noms(nom, prenom) VALUES(?, ?);");
            preparedStatement.setString(1, user.getFirstname());
            preparedStatement.setString(2, user.getLastname());

            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
