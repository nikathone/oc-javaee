<html>
<head>
    <title>Test</title>
</head>
<body>
    <%@ include file="menu.jsp"%>
    <%--<p>Bonjour ${ author.name } ${ 2 * 6 }</p>--%>
    <p>
        <%--Not recommended to add Java code into JSP instesd use JSTL--%>
        <%--<%--%>
            <%--String variable = (String) request.getAttribute("variable");--%>
            <%--out.println(variable);--%>
        <%--%>--%>
    </p>
    <%--<p>Hey person ${ !empty name ? name : 'Bingo' }</p>--%>

    <%--JSPL--%>
    <%--<p><c:out value="Bonjour JSPL!"/></p>--%>
    <%--Testing if a variable is empty then provide a default value.--%>
    <%--A escapeXml="false" can be set to stop jstl from escaping special chars--%>
    <%--<p><c:out value="${ empty_var }" default="Empty value for empty var!"/></p>--%>
    <%--<p><c:out value="${ empty_var }">Empty value for empty var! - another syntax</c:out></p>--%>
    <%--Setting up variable.--%>
    <%--<c:set var="pseudo" value="Nikathone" scope="page" />--%>
    <%--<p><c:out value="${ pseudo }" /></p>--%>
    <%--or--%>
    <%--<c:set var="pseudo2" scope="page">Nikathone</c:set>--%>
    <%--<p><c:out value="${ pseudo2 }" /></p>--%>
    <%--Changing value of a bin--%>
    <%--<c:set target="${ author }" property="name" value="New name" />--%>
    <%--<p>Overwritten name property <c:out value="${ author.name }" /></p>--%>
    <%--Removing variable from memory--%>
    <%--<c:remove var="pseudo" scope="page" />--%>

    <%--Conditions--%>
    <%--<c:if test="${ 50 > 10 }" var="variabletrue">--%>
        <%--<p>C'est vrai !</p>--%>
    <%--</c:if>--%>
    <%--<c:if test="${ 50 < 10 }" var="variablefaux">--%>
        <%--<p>C'est faux won't display !</p>--%>
    <%--</c:if>--%>
    <%--Pour de multiple condition checks--%>
    <%--<c:choose>--%>
        <%--<c:when test="${ variable }">Du Texte</c:when>--%>
        <%--<c:when test="${ otherVariable }">Du Textea</c:when>--%>
        <%--<c:when test="${ otherVvariable }">Du Texter</c:when>--%>
        <%--<c:otherwise>This one!</c:otherwise>--%>
    <%--</c:choose>--%>
    <%-- Boucles --%>
    <%--<c:forEach var="i" begin="0" end="10" step="1">--%>
        <%--<p>Un message #<c:out value="${ i }" /></p>--%>
    <%--</c:forEach>--%>
    <%--<c:forEach items="${ titres }" var="titre" varStatus="status">--%>
        <%--<h4>#<c:out value="${ status.count }"/>: <c:out value="${ titre }" /> - titre</h4>--%>
    <%--</c:forEach>--%>
    <%--<c:forTokens var="morceau" items="Un élément/Encore un autre élément/Un dernier pour la route" delims="/ ">--%>
        <%--<p>${ morceau }</p>--%>
    <%--</c:forTokens>--%>

    <%--<form method="post" action="bonjour">--%>
        <%--<p>--%>
            <%--<label for="login">Login: </label>--%>
            <%--<input type="text" id="login" name="login" />--%>
        <%--</p>--%>
        <%--<p>--%>
            <%--<label for="pass">Password: </label>--%>
            <%--<input type="password" id="pass" name="pass" />--%>
        <%--</p>--%>

        <%--<input type="submit" />--%>
    <%--</form>--%>
    <%--<c:if test="${ !empty form }">--%>
        <%--<hr/>--%>
        <%--<p><c:out value="${ form.resultat }" /></p>--%>
    <%--</c:if>--%>

    <%--<c:if test="${ !empty sessionScope.prenom && !empty sessionScope.nom }">--%>
        <%--<p>Vous etes ${ sessionScope.prenom } ${ sessionScope.nom }</p>--%>
    <%--</c:if>--%>
    <%--<form method="post" action="bonjour">--%>
        <%--<p>--%>
            <%--<label for="nom">Nom: </label>--%>
            <%--<input type="text" id="nom" name="nom" />--%>
        <%--</p>--%>
        <%--<p>--%>
            <%--<label for="prenom">Prenom: </label>--%>
            <%--<input type="text" id="prenom" name="prenom" />--%>
        <%--</p>--%>

        <%--<input type="submit" />--%>
    <%--</form>--%>
    <%--<c:if test="${ !empty form }">--%>
        <%--<hr/>--%>
        <%--<p><c:out value="${ form.resultat }" /></p>--%>
    <%--</c:if>--%>

    <form method="post" action="bonjour">
        <p>
            <label for="nom">Nom: </label>
            <input type="text" id="nom" name="nom" />
        </p>
        <p>
            <label for="prenom">Prenom: </label>
            <input type="text" id="prenom" name="prenom" />
        </p>

        <input type="submit" />
    </form>

    <ul>
        <c:forEach items="${ users }" var="user">
            <li><c:out value="${ user.firstname }" /> <c:out value="${ user.lastname }" /></li>
        </c:forEach>
    </ul>
</body>
</html>
